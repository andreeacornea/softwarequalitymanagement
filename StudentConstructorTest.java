
import static org.junit.Assert.*;

import org.junit.Test;


public class StudentConstructorTest {

	
	@Test
	public void testParamConstructor() {
		String name = "Maria";
		
		Student student2 = new Student (null, name,23);
		assertEquals(name, student2.getName());
	}
	
	@Test
	public void testConstructor() {
		Student student1 = new Student ();
		student1.setName("Ion");
		
		Student student2 = new Student (null, "Ion",23);
		assertEquals(student2.getName(), student1.getName());
	}

}
