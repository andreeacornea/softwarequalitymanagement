

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

public class ExamTest {

	Student student;
	
	
	@Before
	public void setUp() throws Exception {
		ArrayList<Integer> grades = new ArrayList<>();
		grades.add(7);
		student = new Student(grades,"Maria",23);
		student.insertGrade(8);
	}
	
	@Test
	public void testAddNewGradeRight() throws Exception {
		
		int existingNoGrades = student.getGrades().size();
		
		student.insertGrade(5);
		
		assertEquals(existingNoGrades+1, student.getGrades().size());

	}

}
