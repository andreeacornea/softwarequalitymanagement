
import java.util.ArrayList;

public class Student {
	ArrayList<Integer> grades;
	String name;
	int age;
	
	public static final int MIN_AGE = 19;
	public static final int MAX_AGE = 25;
	
	public Student(ArrayList<Integer> grades, String name, int age) {
		this.grades = grades;
		this.name = name;
		this.age = age;
	}
	
	public Student() {
		this.grades = null;
		this.name = "John Doe";
		this.age = 0;
	}

	public ArrayList<Integer> getGrades() {
		return grades;
	}

	public void setGrades(ArrayList<Integer> grades) throws Exception {
		if(grades == null)
			throw new Exception();
		this.grades = (ArrayList<Integer>)grades;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) throws Exception{
		if(age < Student.MIN_AGE)
			throw new Exception();
		this.age = age;
	}
	
	public int getMinGrade() throws Exception{
		if(this.grades == null)
			throw new Exception();
		if(this.grades.size() == 0)
			throw new Exception();
		
		int minimum = this.grades.get(0);
		for(int grade : this.grades)
			if(minimum > grade)
				minimum = grade;
		return minimum;
	}
	
	
	public int addNumbers(int a, int b) {
		return a + b;
	}
	
	
	public void insertGrade(int newGrade) 
			throws Exception {
		if(newGrade == -1)
			throw new Exception();
		this.grades.add(newGrade);
	}

}