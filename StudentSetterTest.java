import static org.junit.Assert.*;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;


public class StudentSetterTest {

	Student student;
	
	@Before
	public void setUp() {
		student = new Student();
	}
	
	@Test
	public void testSetAgeNormalValues() throws Exception {
		int normalAge = 23;
		student.setAge(normalAge);
		
		int actualAge = student.getAge();
		assertEquals(normalAge, actualAge);
		
	}
	
	@Test
	public void testSetAgeLowerLimit() {
		try {
			student.setAge(Student.MIN_AGE);
			int actualAge = student.getAge();
			assertEquals(Student.MIN_AGE, actualAge);
		} catch (Exception e) {
			fail("Student is not compatible");
		}
	}
	
	@Test
	public void testSetAgeUpperLimit() {
		try {
			student.setAge(Student.MAX_AGE);
			int actualAge = student.getAge();
			assertEquals(Student.MAX_AGE, actualAge);
		} catch (Exception e) {
			fail("Student is not compatible");
		}
	}
	
}
