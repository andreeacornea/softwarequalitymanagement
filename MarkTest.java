
import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

public class MarkTest {

	Student student;
	
	@Before
	public void setUp() throws Exception {
		ArrayList<Integer> grades = new ArrayList<>();
		grades.add(7);
		grades.add(8);
		student = new Student(grades,"Maria",23);
		student.insertGrade(8);
	}
	
	@Test
	public void testMinGrade() throws Exception {
		
		int newGrade = 5;
		student.insertGrade(newGrade);
		int minGrade = student.getMinGrade();
		assertEquals("verify min grade",newGrade, minGrade);

	}
}
