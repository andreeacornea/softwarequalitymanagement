
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(Suite.class)
@SuiteClasses({ StudentConstructorTest.class, StudentSetterTest.class })
public class StudentTest {

}
